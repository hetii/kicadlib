#!/usr/bin/env python
import os, re
import glob
import errno 
import shutil
from collections import defaultdict

fp_table_file =  os.environ['HOME']+"/.config/kicad/fp-lib-table"

def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise

def gen_fp_table(paths):
    l = []
    for path in paths:
	modules = glob.glob("%s/modules/*.pretty" % path)
        for module in modules:
            d = {'fp_type':'KiCad'}
            d['fp_name'] = os.path.basename(module).partition('.')[0]
            d['fp_path'] = module
            l.append(d)
    print l
    return l
		
def save_fp_table_file(content):
    fp_header = "(fp_lib_table\n"
    fp_format = "  (lib (name %(fp_name)s)(type %(fp_type)s)(uri %(fp_path)s)(options \"\")(descr \"\"))\n"
    fh = open(fp_table_file, 'w')
    fh.write(fp_header)
    for entry in content:
        fh.write(fp_format % entry)
    fh.write(")\n")
    fh.close();

def find_3d_models(content):
    packages3d_src = '/home/ania/workspace/remote/usr/share/kicad/modules/packages3d/'
    #packages3d_src = "/usr/share/kicad/modules/packages3d/"

    for entry in content:
        module_path = entry['fp_path'].replace("//","/")                    # ./simcad/modules/Capacitors_THT.pretty
        packages3d_dst = "%s/packages3d"  % os.path.dirname(module_path)
        modules = glob.glob("%s/*.kicad_mod" % module_path)

        for module in modules:
            model = re.findall("model ([-/.0-9a-zA-Z_]+)", file(module).read())
            model_path = ''
            if len(model) > 0:
                model_path = model[0]
                packages3d_src_file = "%s/%s" % (packages3d_src, model_path)
                if os.path.isfile(packages3d_src_file):
                    packages3d_dst_path = "%s/%s" % (packages3d_dst, os.path.dirname(model_path))
                    if not os.path.isdir(packages3d_dst_path):
                       mkdir_p(packages3d_dst_path)
                    shutil.copy2(packages3d_src_file, packages3d_dst_path)
                    print "Copy: %s %s" % (packages3d_src_file, packages3d_dst_path)
          
def main():
    content = gen_fp_table(['/home/dev/workspace/kicadlib/'])
    #content = gen_fp_table(['/home/dev/workspace/simcad/'])
    find_3d_models(content)
    save_fp_table_file(content)

    #gen_fp_table(['/usr/share/kicad', '/home/dev/workspace/simcad/'])

if __name__ == '__main__':
	main()
